﻿using System;
using System.IO;


namespace BS.Plugin.NopStation.MobileWebApi.Extensions
{
   public class Loghelper
    {
        public static int Logdetails (string logdetails, int LineNumber)
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("C:\\Windows\\Temp\\Logdetails.txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter(objFilestream);
                objStreamWriter.WriteLine(logdetails, LineNumber);
                objStreamWriter.Close();
                objFilestream.Close();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
           
        }
    }
}
