﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.Extensions.Configuration;
using BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Areas.Admin.Helpers;

namespace BS.Plugin.NopStation.MobileWebApi.Extensions
{

    /// <summary>
    /// Select list helper  
    /// </summary>
    public class ProductqtycheckAPI
    {
        private readonly IConfiguration _configuration;
        public ProductqtycheckAPI(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public GeneralResponseModel<bool> checkqtyProductAPI(IList<int> productid)
        {
           
            GeneralResponseModel<bool> result = new GeneralResponseModel<bool>();
            try
            {
                string joined = string.Join(",", productid);
                Loghelper.Logdetails(joined, 28);
                SqlParameter[] aSqlParameter = new SqlParameter[1];
                aSqlParameter[0] = new SqlParameter("@productid", joined);
                DataTable dt = DirectSQLCommand.ExecuteProcedureDataTable("Product_List", aSqlParameter);
                var emp = (from DataRow row in dt.Rows
                           select new
                           {
                               erpProductId = row["CGMItemID"].ToString(),
                           }).ToList();

                string json = JsonConvert.SerializeObject(emp, Formatting.Indented);
                Loghelper.Logdetails(json, 28);
                string GETERPApi = _configuration.GetValue<string>("ERPApi1:Api1");
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GETERPApi);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }
                System.Net.ServicePointManager.Expect100Continue = false;
                try
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    var responseString = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                    ProjectLogHelper.LogHelper(GETERPApi, json, responseString,"");
                    var myDeserializedClass = JsonConvert.DeserializeObject<List<RootErpQTY>>(responseString);
                    if (myDeserializedClass != null && myDeserializedClass.Count > 0)
                    {
                        var checklessqty = myDeserializedClass.Where(b => b.IsAvailabe == false);
                        if (checklessqty != null && checklessqty.Count() > 0)
                        {
                            result.Data = false;
                        }
                        else
                        {
                            result.Data = true;
                        }
                    }
                    else
                    {
                        result.Data = false;
                    }
                }
                catch (Exception ex)
                {
                    ProjectLogHelper.LogHelper(GETERPApi, json, ex.ToString(), "");
                    result.Data = false;
                    result.StatusCode = (int)ErrorType.NotOk;
                    result.ErrorList.Add(ex.Message);
                }
                return result;
            }

            catch (Exception exc)
            {
               
                result.Data = false;
                result.StatusCode = (int)ErrorType.NotOk;
                result.ErrorList.Add(exc.Message);
                return result;
            }

        }
        public class RootErpQTY
        {
            public string ProductBarcode { get; set; }
            public string ERPProductId { get; set; }
            public string ProductName { get; set; }
            public int Qty { get; set; }
            public int AvailableQty { get; set; }
            public bool IsAvailabe { get; set; }
            public double Rate { get; set; }
        }

    }
}

