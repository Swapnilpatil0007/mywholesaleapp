﻿using BS.Plugin.NopStation.MobileWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using System;
using System.IO;

namespace BS.Plugin.NopStation.MobileWebApi.Controllers
{
    public class StudentApiController : BaseApiController
    {
        private readonly IStudentServiceApi _studentService;
        private readonly INopFileProvider _fileProvider;

        public StudentApiController(IStudentServiceApi studentServiceapi, INopFileProvider fileProvider)
        {
            _studentService = studentServiceapi;
            _fileProvider = fileProvider;
        }
        [Route("api/Student/GetStudents")]
        [HttpGet]
        public IActionResult GetStudents()
        {
            var result = _studentService.GetStudents();
            return Ok(result);
        }
        [Route("api/Student/Insertdata")]
        [HttpPost]
        public virtual IActionResult Insertdata(Student_New1 stud)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);
            string filepath = "";
            string uniquefilename = "";
            if (stud.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + stud.ProfileImage.FileName;
                filepath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    stud.ProfileImage.CopyTo(filestream);
                }
            }

            Student student = new Student();
            //student.Id = stud.Id;
            student.Name = stud.Name;
            student.Email = stud.Email;
            student.Salary = stud.Salary;
            student.ProfileImage = uniquefilename;

            _studentService.InsertStudRecord(student);
            var result = "Inserted";
            return Ok(result);
        }
        [Route("api/Student/Updatedata")]
        [HttpPut]
        public virtual IActionResult Updatedata(Student_New1 stud)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);
            string filepath = "";
            string uniquefilename = "";
            if (stud.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + stud.ProfileImage.FileName;
                filepath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    stud.ProfileImage.CopyTo(filestream);
                }
            }

            Student student = new Student();
            student.Id = stud.Id;
            student.Name = stud.Name;
            student.Email = stud.Email;
            student.Salary = stud.Salary;
            student.ProfileImage = uniquefilename;

            _studentService.UpdateStudRecoed(student);
            var result = "Updated";
            return Ok(result);
        }
        [Route("api/Student/Deletedata/{Id}")]
        [HttpPost]
        public virtual IActionResult Deletedata(Student stud)
        {
            _studentService.DeleteEmpRecord(stud);
            var result = "Deleted";
            return Ok(result);
        }
        [Route ("api/Student/GetStudentById/{Id}")]
        [HttpGet]
        public virtual IActionResult GetStudentById(Student studid)
        {
            //Student stud = new Student();
           // stud.Id = id;
            var result=_studentService.GetStudentSelect(studid);
            return Ok(result);
        }
    }  
}
