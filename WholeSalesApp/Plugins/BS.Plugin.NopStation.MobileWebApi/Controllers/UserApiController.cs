﻿using BS.Plugin.NopStation.MobileWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using System;
using System.IO;

namespace BS.Plugin.NopStation.MobileWebApi.Controllers
{
    public class UserApiController : BaseApiController
    {
        private readonly IUserServiceApi _userService;
        private readonly INopFileProvider _fileProvider;

        public UserApiController(IUserServiceApi userServiceapi, INopFileProvider fileProvider)
        {
            _userService = userServiceapi;
            _fileProvider = fileProvider;
        }
        [Route("api/User/GetAllUsers")]
        [HttpGet]
        public IActionResult GetAllUsers()
        {
            var result = _userService.GetUsers();
            return Ok(result);
        }
        [Route("api/User/AddNewUser")]
        [HttpPost]
        public virtual IActionResult AddNewUser(UserTable1 user)
        {

            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);
            string filepath = "";
            string uniquefilename = "";
            if (user.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + user.ProfileImage.FileName;
                filepath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    user.ProfileImage.CopyTo(filestream);
                }
            }

            UserTable newuser = new UserTable();
            newuser.Name = user.Name;
            newuser.Email = user.Email;
            newuser.Mobile = user.Mobile;
            newuser.CreatedDate = DateTime.UtcNow;
            newuser.Gender = user.Gender;
            newuser.MaritalStatus = user.MaritalStatus;
            newuser.ProfileImage = uniquefilename;

            _userService.InsertUserRecord(newuser);
            var result = "Inserted";
            return Ok(result);
        }
        [Route("api/User/Updatedata")]
        [HttpPut]
        public virtual IActionResult Updatedata(UserTable1 user)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);
            string filepath = "";
            string uniquefilename = "";
            if (user.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + user.ProfileImage.FileName;
                filepath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    user.ProfileImage.CopyTo(filestream);
                }
            }

            UserTable updateuser = new UserTable();
            updateuser.Id = user.Id;
            updateuser.Name = user.Name;
            updateuser.Email = user.Email;
            updateuser.Mobile = user.Mobile;
            updateuser.CreatedDate = DateTime.UtcNow;
            updateuser.Gender = user.Gender;
            updateuser.MaritalStatus = user.MaritalStatus;
            updateuser.ProfileImage = uniquefilename;

            _userService.UpdateUserRecoed(updateuser);
            var result = "Updated";
            return Ok(result);
        }
        [Route("api/User/Deletedata/{Id}")]
        [HttpPost]
        public virtual IActionResult Deletedata(UserTable userid)
        {
            _userService.DeleteUserRecord(userid);
            var result = "Deleted";
            return Ok(result);
        }
        //[Route ("api/Student/GetStudentById/{Id}")]
        //[HttpGet]
        //public virtual IActionResult GetStudentById(Student studid)
        //{
        //    //Student stud = new Student();
        //   // stud.Id = id;
        //    var result=_studentService.GetStudentSelect(studid);
        //    return Ok(result);
        //}
    }  
}
