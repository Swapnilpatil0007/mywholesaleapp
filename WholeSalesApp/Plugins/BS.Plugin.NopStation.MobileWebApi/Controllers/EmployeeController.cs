﻿using Microsoft.AspNetCore.Mvc;
using BS.Plugin.NopStation.MobileWebApi.Services;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using System;
using System.IO;

namespace BS.Plugin.NopStation.MobileWebApi.Controllers
{
    // Created by Alexandar Rajavel on 11-July-2019
    public class EmployeeController : BaseApiController
    {
        private readonly IEmployeeServiceApi _employeeService;
        private readonly INopFileProvider _fileProvider;

        public EmployeeController(IEmployeeServiceApi employeeServiceApi,
            INopFileProvider fileProvider)
        {
            _employeeService = employeeServiceApi;
            _fileProvider = fileProvider;
        }


        [Route("api/Employee/GetEmployee")]
        [HttpGet]
        public IActionResult GetEmployee()
        {
            var result = _employeeService.GetEmployee();
            return Ok(result);
        }

        [Route("api/Employee/Insertdata")]
        [HttpPost]
        public virtual IActionResult Insertdata([FromBody] Employee_New1 emp1)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniqueFileName = "";
         
            if (emp1.ProfileImage != null)
            {
                uniqueFileName = Guid.NewGuid().ToString() + "_" + emp1.ProfileImage.FileName;
                filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    emp1.ProfileImage.CopyTo(fileStream);
                }
            }
            Employee_New emp = new Employee_New();

            emp.Name = emp1.Name;
            emp.Email = emp1.Email;
            emp.Salary = emp1.Salary;
            emp.ProfileImage = uniqueFileName;

            _employeeService.InsertEmpRecord(emp);

            var result = "Inserted";
            return Ok(result);
        }

        [Route("api/Employee/Updatedata")]
        [HttpPut]
        public IActionResult Updatedata(Employee_New1 emp1)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniqueFileName = "";

            if (emp1.ProfileImage != null)
            {
                uniqueFileName = Guid.NewGuid().ToString() + "_" + emp1.ProfileImage.FileName;
                filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    emp1.ProfileImage.CopyTo(fileStream);
                }
            }
            Employee_New emp = new Employee_New();
            emp.Id = emp1.Id;
            emp.Name = emp1.Name;
            emp.Email = emp1.Email;
            emp.Salary = emp1.Salary;
            emp.ProfileImage = uniqueFileName;

            _employeeService.UpdateEmpRecord(emp);
            // var result = _employeeService.InsertEmpRecord(emp);
            var result = "Updated";
            return Ok(result);
        }

        [Route("api/Employee/Deletedata/{Id}")]
        [HttpPost]
        public IActionResult Deletedata(Employee_New empId)
        {
            var result = _employeeService.DeleteEmpRecord(empId);
            return Ok(result);
        }
        [Route("api/Employee/GetEmployeeById/{Id}")]
        [HttpGet]
        public IActionResult GetEmployeeById(int Id)
        {
            Employee_New empid = new Employee_New();
            empid.Id = Id;
            var result = _employeeService.GetEmployeeSelect(empid);
            return Ok(result);
        }

    }
}
