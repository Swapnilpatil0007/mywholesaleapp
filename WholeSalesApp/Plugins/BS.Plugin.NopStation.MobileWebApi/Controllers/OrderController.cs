﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using BS.Plugin.NopStation.MobileWebApi.Factories;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Microsoft.AspNetCore.Mvc;
using BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel.Order;
using BS.Plugin.NopStation.MobileWebApi.Extensions;
using BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel.Customer;
using BS.Plugin.NopStation.MobileWebApi.Models._QueryModel.Order;
using Nop.Core.Domain.Messages;
using BS.Plugin.NopStation.MobileWebApi.Services;
using Nop.Services.Messages;
using Microsoft.Extensions.Configuration;
using BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel.ERPUpdate;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Nop.Web.Areas.Admin.Helpers;

namespace BS.Plugin.NopStation.MobileWebApi.Controllers
{
    public partial class OrderController : BaseApiController
    {
        #region Fields
        private readonly IDeviceService _deviceService;
        private readonly IConfiguration _configuration;
        private readonly INotificationService _notificationService;
        private IOrderModelFactoryApi _orderModelFactoryApi;
        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPaymentService _paymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IPdfService _pdfService;
        private readonly IShippingService _shippingService;
        private readonly ICountryService _countryService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IWebHelper _webHelper;
        private readonly IDownloadService _downloadService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IStoreContext _storeContext;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IPictureService _pictureService;
        private readonly OrderSettings _orderSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PdfSettings _pdfSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Constructors

        public OrderController(IOrderModelFactoryApi orderModelFactoryApi,
            IOrderService orderService,
            IShipmentService shipmentService,
            IWorkContext workContext,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService,
            IDateTimeHelper dateTimeHelper,
            IPaymentService paymentService,
            ILocalizationService localizationService,
            IPdfService pdfService,
            IShippingService shippingService,
            ICountryService countryService,
            IProductAttributeParser productAttributeParser,
            IWebHelper webHelper,
            IDownloadService downloadService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IStoreContext storeContext,
            IOrderTotalCalculationService orderTotalCalculationService,
            CatalogSettings catalogSettings,
            OrderSettings orderSettings,
            TaxSettings taxSettings,
            ShippingSettings shippingSettings,
            AddressSettings addressSettings,
            RewardPointsSettings rewardPointsSettings,
            PdfSettings pdfSettings,
            IPictureService pictureService,
            MediaSettings mediaSettings,
            ICacheManager cacheManager,
            IDeviceService deviceService,
            INotificationService notificationService,
            IConfiguration iconfig)
        {
            this._orderModelFactoryApi = orderModelFactoryApi;
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._dateTimeHelper = dateTimeHelper;
            this._paymentService = paymentService;
            this._localizationService = localizationService;
            this._pdfService = pdfService;
            this._shippingService = shippingService;
            this._countryService = countryService;
            this._productAttributeParser = productAttributeParser;
            this._webHelper = webHelper;
            this._downloadService = downloadService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._storeContext = storeContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
            _deviceService = deviceService;
            _notificationService = notificationService;
            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._taxSettings = taxSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._pdfSettings = pdfSettings;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
            this._cacheManager = cacheManager;
            this._configuration = iconfig;

        }

        #endregion



        #region Methods

        //My account / Orders api
        [HttpGet]
        [Route("api/order/customerorders")]
        public IActionResult CustomerOrders()
        {
            // Commented by Alexandar Rajavel on 06-Mar-2019 for Guest user
            //if (!_workContext.CurrentCustomer.IsRegistered())
            //    return Challenge(HttpStatusCode.Unauthorized.ToString());

            var model = _orderModelFactoryApi.PrepareCustomerOrderListModel();
            return Ok(model);
        }


        [HttpGet]
        [Route("api/order/details/{orderId}")]
        public IActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Unauthorized();

            var model = _orderModelFactoryApi.PrepareOrderDetailsModel(order);
            return Ok(model);
        }

        public void ErrorLog(string log) 
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("C:\\Windows\\Temp\\Log.txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter(objFilestream);
                objStreamWriter.WriteLine(log);
                objStreamWriter.Close();
                objFilestream.Close();
            }
            catch (Exception)
            {
 
            }
        }

        [HttpPost]
        [Route("api/order/UpdateOrderStatus/{orderId}/{PGName}/{TransID}")]
        public IActionResult UpdateOrderStatus(int orderId, string PGName, string TransID)
        {

           ErrorLog("Call:UpdateOrderStatus");
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
                return Unauthorized();
            order.PaymentStatusId = 30;
            order.PaymentMethodSystemName = PGName;
            order.AuthorizationTransactionId = TransID;

            _orderService.UpdateOrder(order);

            //POS_PAYMENT_TYPE_CASH_ID = "1461d99f-d8be-46f3-9927-138fef405886";
            //POS_PAYMENT_TYPE_CREDIT_CARD_ID = "ac6a233f-ba93-435a-bd07-561b26952304";
            //POS_PAYMENT_TYPE_OKDOLLAR_ID = "017fe3b9-30dc-4814-a657-76ec79088966";

            // Marked as hardcoded, need to make dynamic or put in appsettings.json
            var paymentstatusid = string.Empty;

            if (PGName == "OK$")
            {
                paymentstatusid = _configuration.GetValue<string>("ERPOK:POS_PAYMENT_TYPE_OKDOLLAR_ID"); //"017fe3b9-30dc-4814-a657-76ec79088966";
            }
            else if (PGName == "CASH")
            {
                paymentstatusid = _configuration.GetValue<string>("ERPCASH:POS_PAYMENT_TYPE_CASH_ID"); //"1461d99f-d8be-46f3-9927-138fef405886";
            }
            else
            {
                paymentstatusid = _configuration.GetValue<string>("ERPOther:POS_PAYMENT_TYPE_CREDIT_CARD_ID");// "ac6a233f-ba93-435a-bd07-561b26952304";
            }
            string EnableERPInvoice = _configuration.GetValue<string>("EnableERPInvoice:Status");

            if (EnableERPInvoice == "1")
            {
                ErrorLog("(EnableERPInvoice=1");

                string ERPCompanyID = _configuration.GetValue<string>("ERPCompanyID:CompanyID");
                string ERPBusinessID = _configuration.GetValue<string>("ERPBusinessID:BusinessID");

                //for Posting detials to erp server Added By Priti
                List<POSSalesInvoiceDetailModel> orderItems = new List<POSSalesInvoiceDetailModel>();
                
                    foreach (OrderItem orderitem in order.OrderItems)
                    {
                        if (orderitem.Product.CGMItemID != null)
                        {
                            int mainquantity = 0;
                            string mynumber = Regex.Replace(orderitem.AttributeDescription, @"\D", "");
                            if (mynumber != null && mynumber != "")
                            {
                                if (Convert.ToInt32(mynumber) > 0)
                                {
                                    //mainquantity = orderitem.Quantity;
                                    mainquantity = (Convert.ToInt32(mynumber) * orderitem.Quantity);
                                }
                                else
                                {
                                    mainquantity = orderitem.Quantity;
                                }
                            }
                            else
                            {
                                mainquantity = orderitem.Quantity;
                            }

                            POSSalesInvoiceDetailModel orderItem = new POSSalesInvoiceDetailModel
                            {
                                ProductId = Convert.ToString(orderitem.Product.CGMItemID),
                                Quantity = mainquantity,
                                SellingRate = orderitem.UnitPriceInclTax,
                                ecommid = orderitem.Id,

                            };
                            orderItems.Add(orderItem);
                        }
                    }

                


                if (orderItems.Count > 0)
                {
                    ErrorLog("orderItems");
                    POSSalesInvoiceHeaderModel processPaymentOrderDetail = new POSSalesInvoiceHeaderModel
                    {
                        CompanyId = ERPCompanyID,
                        BusinessUnitId = ERPBusinessID,
                        InvoiceDate = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"),// order.CreatedOnUtc.ToString("yyyy-MM-dd HH:mm:ss"),

                        TotalAmount = order.OrderTotal,
                        SalesInvoiceCode = order.CustomOrderNumber,
                        IsWholeSale = true,
                        Remarks = order.Customer.BillingAddress.FirstName + order.Customer.BillingAddress.LastName + "|" + order.Customer.Username + "|" + order.StoreName
                    };

                    List<SalesInvoicePayment> salesInvoicePayment = new List<SalesInvoicePayment>();


                    SalesInvoicePayment sw = new SalesInvoicePayment();
                    sw.CompanyId = ERPCompanyID;
                    sw.BusinessUnitId = ERPBusinessID;
                    sw.PaymentTypeId = paymentstatusid;
                    sw.CardType = null;
                    sw.BankId = null;
                    sw.CardNo = null;
                    sw.TransactionNo = TransID;
                    sw.Amount = order.OrderTotal;

                    salesInvoicePayment.Add(sw);
              
                    RootObjectModel possalesorder = new RootObjectModel
                    {
                        POSSalesInvoiceHeaderModel = processPaymentOrderDetail,
                        POSSalesInvoiceDetail = orderItems,
                        SalesInvoicePayment = salesInvoicePayment
                    };

                    string GETERPApi = _configuration.GetValue<string>("eCommerceInvoicePayment:Api");
                    string json = JsonConvert.SerializeObject(possalesorder, Formatting.Indented);


                string EnableLogs = _configuration.GetValue<string>("EnableLogs:Status");

                if (EnableLogs == "1")
                {
                        ErrorLog("EnableLogs=1");
                        FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", Path.GetTempPath(),
      "ConsoleCheckoutPage"), FileMode.Append, FileAccess.Write);
                    StreamWriter objStreamWriter = new StreamWriter(objFilestream);
                    objStreamWriter.WriteLine(json);
                    objStreamWriter.Close();
                    objFilestream.Close();
                }


                    ErrorLog(GETERPApi);
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GETERPApi);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                    }
                    System.Net.ServicePointManager.Expect100Continue = false;

                    try
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        //log add 07/06/2022
                        string CreateItemId = null;
                        string responseString = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                        ProjectLogHelper.LogHelper(GETERPApi, json, responseString, CreateItemId);
                        //log end
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            ErrorLog(result.ToString());
                        }

                    }
                    catch (Exception ex)
                    {
                        //log add 07/06/2022
                        string CreateItemId = null;
                        ProjectLogHelper.LogHelper(GETERPApi, json, ex.Message.ToString(), CreateItemId);
                        //log end

                        ErrorLog(ex.Message);
                        if (EnableLogs == "1")
                        {
                            FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", Path.GetTempPath(),
              "ErrorERPinvoiceSent"), FileMode.Append, FileAccess.Write);
                            StreamWriter objStreamWriter = new StreamWriter(objFilestream);
                            objStreamWriter.WriteLine(ex.Message);
                            objStreamWriter.Close();
                            objFilestream.Close();
                        }

                    }




                }
            }


  

            var response = new CommonResponseModel<bool>()
            {
                Data = true,
                StatusCode = 200,
                SuccessMessage="Order Process Compelete"
                
            };

            return Ok(response);
        }

        // For Ware house management
        [HttpGet]
        [Route("api/order/detailsforwhm/{orderId}")]
        public IActionResult DetailsForWHM(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            var response = new NewRegisterResponseModel<OrderDetailsForWHM>();
            if (order == null || order.Deleted)
            {
                response.StatusCode = (int)ErrorType.NotFound;
                response.ErrorList.Add(ID_NOT_FOUND);
            }
            else
            {
                response.SuccessMessage = SUCCESS;
                response.Data = _orderModelFactoryApi.PrepareOrderDetailsModelForWHM(order);
            }
            return Ok(response);
        }


        ////My account / Order details page / PDF invoice
        [HttpGet]
        [Route("api/order/getpdfinvoice/{orderId}")]
        //My account / Order details page / PDF invoice
        public virtual IActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, MimeTypes.ApplicationPdf, $"order_{order.Id}.pdf");
        }

        ////My account / Order details page / re-order
        [HttpGet]
        [Route("api/order/reorder/{orderId}")]
        public IActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var response = new GeneralResponseModel<bool>()
            {
                Data = true
            };
            _orderProcessingService.ReOrder(order);

            return Ok(response);
        }




        [HttpGet]
        [Route("api/order/shipmentdetails/{shipmentId}")]
        public IActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return Challenge();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();
            var model = _orderModelFactoryApi.PrepareShipmentDetailsModel(shipment);

            return Ok(model);

        }

        /// Created By: Sunil Kumar
        /// Created on : 3 Oct 2018
        /// Get Order List By Delivery BoyId
        [HttpGet]
        [Route("api/order/GetOrderListByDeliveryBoyId/{deliveryBoyId}")]
        public IActionResult GetOrderListByDeliveryBoyId(int deliveryBoyId)
        {
            var order = _orderService.GetOrderListByDeliveryBoyId(deliveryBoyId);
            if (order == null)
                return Challenge(HttpStatusCode.Unauthorized.ToString());
            var model = _orderModelFactoryApi.PrepareOrderDetailsModel(order);

            return Ok(model);
        }

        /// Created By: Sunil Kumar
        /// Created on : 2nd Jan 2020
        //Delivery Status from Ware House
        [HttpPost]
        [Route("api/order/DeliveryStatusFromWH")]
        public IActionResult DeliveryStatusFromWH([FromBody]OrderDetailsQueryModel model)
        {
            var orderID = model.OrderID;
            var order = _orderService.GetOrderById(Convert.ToInt32(orderID));
            var response = new NewRegisterResponseModel<DeliveryStatusFromWHResponseModel>();

            var deliveryStatus = new DeliveryStatusFromWHResponseModel();
            if (order == null || order.Deleted || !(model.ReadForDelivery.ToLower() == "true"))
            {
                response.StatusCode = (int)ErrorType.NotFound;
                response.ErrorList.Add(ID_NOT_FOUND);
            }
            else
            {
                deliveryStatus.OrderID = orderID;
                deliveryStatus.SentToDelivery = "true";
                response.SuccessMessage = SUCCESS;
                response.Data = deliveryStatus;
            }
            return Ok(response);
        }

        ////My account / Order details page / re-order
        [HttpPost]
        [Route("api/order/PushNotification/{orderId}")]
        public IActionResult DeliveryPushNotification(int orderId)
        {

            var baseResponse = new BaseResponse();
            var order = _orderService.GetOrderById(orderId);
         

            var deviceDetails = _deviceService.GetDevicesByCustomerId(order.CustomerId);
            if (deviceDetails.Any())
            {
                var notification = new QueuedNotification()
                {
                    DeviceType = (DeviceType)deviceDetails[0].DeviceType,
                    SubscriptionId = deviceDetails[0].SubscriptionId,
                    Message = _localizationService.GetResource("Notification.DeliveryNoti"),
                };
                _notificationService.SendNotication(notification);
                baseResponse.StatusCode = (int)ErrorType.Ok;
                return Ok(baseResponse);
            }
            else
            {
                baseResponse.StatusCode = (int)ErrorType.NotFound;
                baseResponse.ErrorList.Add("Device Not Registered with Mobile App");
                return Ok(baseResponse);
            }
        }
        #endregion
    }
}
