﻿using Nop.Web.Framework.Models;

namespace BS.Plugin.NopStation.MobileWebApi.Models.Vendor
{
    public partial class ProductPictureSearchModel : BaseSearchModel
    {
        #region Properties

        public int ProductId { get; set; }

        #endregion
    }
}
