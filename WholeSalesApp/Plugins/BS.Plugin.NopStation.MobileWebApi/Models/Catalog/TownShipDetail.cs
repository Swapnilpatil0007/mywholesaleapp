﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Models.Catalog
{
    public class TownShipDetail
    {
        public string ID { get; set; }
        public string CityName { get; set; }
        public string MinOrderValue { get; set; }
    }
}
