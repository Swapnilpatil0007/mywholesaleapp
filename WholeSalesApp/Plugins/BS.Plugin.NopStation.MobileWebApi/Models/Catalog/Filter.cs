﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Models.Catalog
{
    public class Filter
    {
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
        public int AttributeValueId { get; set; }

    }
}
