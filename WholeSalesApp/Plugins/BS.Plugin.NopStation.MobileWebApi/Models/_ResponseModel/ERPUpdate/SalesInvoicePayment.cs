﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Models._ResponseModel.ERPUpdate
{
    public partial class SalesInvoicePayment
    {
        public string CompanyId { get; set; }
        public string BusinessUnitId { get; set; }
        public string PaymentTypeId { get; set; }
        public string CardType { get; set; }
        public string BankId { get; set; }
        public string CardNo { get; set; }
        public string TransactionNo { get; set; }
        public decimal Amount { get; set; }


    }
}
