﻿using Microsoft.AspNetCore.Http;
using Nop.Core;
using System.Collections.Generic;


namespace BS.Plugin.NopStation.MobileWebApi.Models.Employee
{
    public partial class EmployeeListmodel
    {

        public List<Employee_New> ListEmployeemodeldata { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
        public string ProfileImage { get; set; }
    }
    public partial class Employee_New : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
        public string ProfileImage { get; set; }
    }
    public partial class Employee_New1
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
        public IFormFile ProfileImage { get; set; }
    }
    //public partial class EmployeeListmodel
    //{

    //    public List<ListEmployeemodel> ListEmployeemodeldata { get; set; }

    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Email { get; set; }
    //    public decimal Salary { get; set; }
    //}
    //public class ListEmployeemodel1
    //{

    //    public string Name { get; set; }
    //    public string Email { get; set; }
    //    public decimal Salary { get; set; }
    //}

    //public class ListEmployeemodel
    //{

    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Email { get; set; }
    //    public decimal Salary { get; set; }
    //}
}
