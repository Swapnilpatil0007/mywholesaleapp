﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Models._QueryModel.Order
{
    public class OrderDetailsQueryModel
    {
        public string OrderID { get; set; }
        public string ReadForDelivery { get; set; }
    }
}
