﻿using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public class UserServiceApi : IUserServiceApi
    {
        private readonly IRepository<UserTable> _UserserviceApiRepository;

        public UserServiceApi(IRepository<UserTable> UserserviceApiRepository)
        {
            _UserserviceApiRepository = UserserviceApiRepository;
        }

        public string DeleteUserRecord(UserTable userid)
        {
            _UserserviceApiRepository.Delete(userid);
            var query = from s in _UserserviceApiRepository.Table where s.Id == userid.Id select s;
            return "Deleted";
        }

        public List<UserTable> GetUsers()
        {
            var query = _UserserviceApiRepository.Table;
            var data = query.ToList();
            return data;
        }

        //public Student GetStudentSelect(Student studid)
        //{
        //    var query = from s in _StudentserviceApiRepository.Table where s.Id == studid.Id select s;
        //    var data = query.ToList().FirstOrDefault();
        //    return data;
        //}

        public string InsertUserRecord(UserTable user)
        {
            _UserserviceApiRepository.Insert(user);
            var query = from s in _UserserviceApiRepository.Table select s;

            return "Inserted";
        }

        public string UpdateUserRecoed(UserTable user)
        {
            _UserserviceApiRepository.Update(user);
            var query = _UserserviceApiRepository.Table;
            var details = query.ToList().FirstOrDefault();
            return "Updated";
        }
        public List<UserTable3> UserSearchModel(UserTable3 usersearch)
        {

            SqlConnection Con = new SqlConnection();
            DataSet ds = null;
            List<UserTable3> list = null;

            try
            {

                // Use For Get
                Con = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                SqlCommand cmd = new SqlCommand("spSearchUsers", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", usersearch.Name);
                cmd.Parameters.AddWithValue("@Email", usersearch.Email);
                cmd.Parameters.AddWithValue("@Mobile", usersearch.Mobile);
                cmd.Parameters.AddWithValue("@Gender", usersearch.Gender);
                cmd.Parameters.AddWithValue("@MaritalStatus", usersearch.MaritalStatus);
                cmd.Parameters.AddWithValue("@PageNumber", usersearch.PageNumber);
                cmd.Parameters.AddWithValue("@PageSize", usersearch.PageSize);
                Con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);

                list = new List<UserTable3>();
                // Use for Set
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    UserTable3 userTable = new UserTable3();
                    userTable.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                    userTable.Name = ds.Tables[0].Rows[i]["Name"].ToString();
                    userTable.Email = ds.Tables[0].Rows[i]["Email"].ToString();
                    userTable.Mobile = ds.Tables[0].Rows[i]["Mobile"].ToString();
                    userTable.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["CreatedDate"].ToString());
                    userTable.Gender = ds.Tables[0].Rows[i]["Gender"].ToString();
                    userTable.MaritalStatus = ds.Tables[0].Rows[i]["MaritalStatus"].ToString();
                    userTable.ProfileImage = ds.Tables[0].Rows[i]["ProfileImage"].ToString();
                    int PageSize = Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    int TotalPages = Convert.ToInt32(ds.Tables[0].Rows[i]["TotalPages"].ToString());
                    userTable.TotalPages = Convert.ToInt32(Math.Ceiling((double)TotalPages / PageSize));
                    userTable.PageSize = Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    userTable.PageNumber = Convert.ToInt32(ds.Tables[0].Rows[i]["PageNumber"].ToString());
                    userTable.CurrentPage = Convert.ToInt32(ds.Tables[0].Rows[i]["CurrentPage"].ToString());
                    list.Add(userTable);
                }
                return list;


            }

            catch
            {
                return list;
            }
            finally
            {
                Con.Close();
            }
        }
    }

}
