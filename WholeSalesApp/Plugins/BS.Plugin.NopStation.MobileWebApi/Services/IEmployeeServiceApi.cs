﻿using Nop.Core.Domain.Catalog;
using System.Collections.Generic;



namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public partial interface IEmployeeServiceApi 
    {
        string InsertEmpRecord(Employee_New emp);

        string UpdateEmpRecord(Employee_New emp);
        string DeleteEmpRecord(Employee_New empId);
        List<Employee_New> GetEmployee();

        Employee_New GetEmployeeSelect(Employee_New empid);

    }
}
