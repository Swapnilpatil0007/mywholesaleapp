﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;

namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public partial interface IStudentServiceApi
    {
        List<Student> GetStudents();

        string InsertStudRecord(Student stud);

        string UpdateStudRecoed(Student stud);
        string DeleteEmpRecord(Student studid);

        Student GetStudentSelect(Student studid);
    }
}
