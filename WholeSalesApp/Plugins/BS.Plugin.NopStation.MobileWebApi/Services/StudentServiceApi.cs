﻿using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public class StudentServiceApi : IStudentServiceApi
    {
        private readonly IRepository<Student> _StudentserviceApiRepository;

        public StudentServiceApi(IRepository<Student> StudentserviceApiRepository)
        {
            _StudentserviceApiRepository = StudentserviceApiRepository;
        }

        public string DeleteEmpRecord(Student studid)
        {
            _StudentserviceApiRepository.Delete(studid);
            var query = from s in _StudentserviceApiRepository.Table where s.Id == studid.Id select s;
            return "Deleted";
        }

        public List<Student> GetStudents()
        {
            //List<Student> students = new List<Student>();
            var query = _StudentserviceApiRepository.Table;
            var data = query.ToList();
            return data;
        }

        public Student GetStudentSelect(Student studid)
        {
            var query = from s in _StudentserviceApiRepository.Table where s.Id == studid.Id select s;
            var data = query.ToList().FirstOrDefault();
            return data;
        }

        public string InsertStudRecord(Student stud)
        {
            _StudentserviceApiRepository.Insert(stud);
            var query = _StudentserviceApiRepository.Table;
            
            return "Inserted";
        }

        public string UpdateStudRecoed(Student stud)
        {
            _StudentserviceApiRepository.Update(stud);
            var query=_StudentserviceApiRepository.Table;
            var details = query.ToList().FirstOrDefault();
            return "Updated";
        }
        
    }
}
