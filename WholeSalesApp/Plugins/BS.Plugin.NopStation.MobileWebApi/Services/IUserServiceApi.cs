﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;

namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public partial interface IUserServiceApi
    {
        List<UserTable> GetUsers();

        string InsertUserRecord(UserTable user);

        string UpdateUserRecoed(UserTable user);
        string DeleteUserRecord(UserTable userid);

        //Student GetStudentSelect(Student studid);
        List<UserTable3> UserSearchModel(UserTable3 usersearch);
    }
}
