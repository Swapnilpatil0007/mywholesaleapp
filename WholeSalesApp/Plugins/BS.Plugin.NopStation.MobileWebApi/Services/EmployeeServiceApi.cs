﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;

namespace BS.Plugin.NopStation.MobileWebApi.Services
{
    public class EmployeeServiceApi : IEmployeeServiceApi
    {
        private readonly IRepository<Employee_New> _ListEmployeemodelRepository;
        public EmployeeServiceApi(IRepository<Employee_New> ListEmployeemodelRepository)
        {
            _ListEmployeemodelRepository = ListEmployeemodelRepository;
        }

        public List<Employee_New> GetEmployee()
        {
            List<Employee_New> employees = new List<Employee_New>();

            var query = _ListEmployeemodelRepository.Table;
            var detail = query.ToList();

            return detail;

        }
        public string InsertEmpRecord(Employee_New emp)
        {
            emp.Id = 0;
            _ListEmployeemodelRepository.Insert(emp);
            var query = from p in _ListEmployeemodelRepository.Table select p;
            return "Insert";
        }

        public string UpdateEmpRecord(Employee_New emp)
        {
            _ListEmployeemodelRepository.Update(emp);
            var query = from p in _ListEmployeemodelRepository.Table select p;
            var detail = query.ToList().FirstOrDefault();
            return "Updated";
        }
        public string DeleteEmpRecord(Employee_New empId)
        {
            _ListEmployeemodelRepository.Delete(empId);
            var query = from p in _ListEmployeemodelRepository.Table where p.Id == empId.Id select p;
            return "Deleted";
        }

        public Employee_New GetEmployeeSelect(Employee_New model)
        {
            var query = from p in _ListEmployeemodelRepository.Table where p.Id == model.Id select p;
            var detail = query.ToList().FirstOrDefault();
            return detail;
        }
    }
}
