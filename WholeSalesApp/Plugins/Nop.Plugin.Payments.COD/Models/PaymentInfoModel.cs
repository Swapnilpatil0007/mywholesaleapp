﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.COD.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public string DescriptionText { get; set; }
    }
}