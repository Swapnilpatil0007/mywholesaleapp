﻿using Microsoft.AspNetCore.Http;
using Nop.Core.Domain.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    public interface IUserModelFactory
    {
        List<UserTable> GetAllUsers();

        void InsertUserRecord(UserTable user);

        UserTable GetUserByEmail(string email);

        UserTable GetUserByMobile(string mobile);
        TestTable GetUserMobile(string mobile);
        //void InsertTestRecord(TestTable user);

        void AddDetails(TestTable user);

        string GetUserMaritalstatus(string mstatus);

        string GetUserGender(string ugender);

        UserTable GetUserSelect(UserTable user);

        UserTable UpdateUserRecord(UserTable user);

        UserTable DeleteUserRecord(UserTable userid);

        List<UserTable3> UserSearchModel(UserTable3 usersearch);
    }
}
