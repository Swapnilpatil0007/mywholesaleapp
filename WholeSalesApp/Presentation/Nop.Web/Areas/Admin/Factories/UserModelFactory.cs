﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using iTextSharp.text;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Employee;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial class UserModelFactory : IUserModelFactory
    {
        private readonly IRepository<UserTable> _UserModelRepo;
        private readonly IRepository<TestTable> _TestTableModelRepo;

        public UserModelFactory(IRepository<UserTable> userModelRepo, IRepository<TestTable> TesttableModelRepo)
        {
            _UserModelRepo = userModelRepo;
            _TestTableModelRepo = TesttableModelRepo;
        }

        public List<UserTable> GetAllUsers()
        {
            var query = from s in _UserModelRepo.Table orderby s.Id descending select s;
            var result = query.ToList();
            return result;
        }

        public void InsertUserRecord(UserTable user)
        {
            _UserModelRepo.Insert(user);
            var query = from s in _UserModelRepo.Table select s;
        }
        public virtual UserTable GetUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;
            //added code for removing deleted customers in get by Sunil Kumar S on 20-03-19
            var query = from c in _UserModelRepo.Table
                        orderby c.Id
                        where c.Email == email
                        select c;
            var user1 = query.FirstOrDefault();
            return user1;
        }
        public virtual UserTable GetUserByMobile(string mobile)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return null;
            //added code for removing deleted customers in get by Sunil Kumar S on 20-03-19
            var query = from c in _UserModelRepo.Table
                        orderby c.Id
                        where c.Mobile == mobile
                        select c;
            var user1 = query.FirstOrDefault();
            return user1;
        }
        public virtual TestTable GetUserMobile(string mobile)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return null;
            //added code for removing deleted customers in get by Sunil Kumar S on 20-03-19
            var query = from c in _TestTableModelRepo.Table
                        orderby c.Id
                        where c.Mobile == mobile
                        select c;
            var user1 = query.FirstOrDefault();
            return user1;
        }
        //public void InsertTestRecord(TestTable user)
        //{
        //    _TestTableModelRepo.Insert(user);
        //    var query = from s in _TestTableModelRepo.Table select s;
        //}
        public void AddDetails(TestTable obj)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
            SqlCommand com = new SqlCommand("AddEmp", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@Gender", obj.Gender);
            com.Parameters.AddWithValue("@Mobile", obj.Mobile);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
        }
        //public virtual UserTable GetUserByImage(string Image)
        //{
        //    if (string.IsNullOrWhiteSpace(Image))
        //        return null;
        //    var query = from c in _UserModelRepo.Table
        //                select c;
        //    var user1 = query.FirstOrDefault();
        //    return user1;
        //}
        public virtual string GetUserMaritalstatus(string mstatus)
        {
            if (string.IsNullOrWhiteSpace(mstatus))
                return null;
            //added code for removing deleted customers in get by Sunil Kumar S on 20-03-19
            //var query = from c in _UserModelRepo.Table
            //            orderby c.Id
            //            where c.MaritalStatus == mstatus
            //            select c;
            //var user1 = query.FirstOrDefault();
            //return user1;

            return string.Empty;
        }
        public virtual string GetUserGender(string ugender)
        {
            if (string.IsNullOrWhiteSpace(ugender))
                return null;
            return string.Empty;
        }
        public UserTable GetUserSelect(UserTable user)
        {            
            var query = from s in _UserModelRepo.Table where s.Id == user.Id select s;
            var result = query.ToList().FirstOrDefault();

            return result;
        }

        public UserTable UpdateUserRecord(UserTable user)
        {
            _UserModelRepo.Update(user);
            var query = from s in _UserModelRepo.Table select s;
            var result = query.ToList().FirstOrDefault();
            return result;
        }

        public UserTable DeleteUserRecord(UserTable userid)
        {
            _UserModelRepo.Delete(userid);
            var query = from s in _UserModelRepo.Table where s.Id == userid.Id select s;
            var result = query.ToList().FirstOrDefault();
            return result;
        }

        public List<UserTable3> UserSearchModel(UserTable3 usersearch)
        {

            SqlConnection Con = new SqlConnection();
            DataSet ds = null;
            List<UserTable3> list = null;

            if(usersearch.PageSize != 0 && usersearch.PageSize != 10)
            {
                usersearch.PageSize = usersearch.PageSize;
            }
            else
            {
                usersearch.PageSize = 10;
            }
            if (usersearch.PageNumber != 0)
            {
                usersearch.PageNumber = usersearch.PageNumber;
            }
            else
            {
                usersearch.PageNumber = 1;
            }
            try
            {


                Con = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                SqlCommand cmd = new SqlCommand("spSearchUsers", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", usersearch.Name);
                cmd.Parameters.AddWithValue("@Email", usersearch.Email);
                cmd.Parameters.AddWithValue("@Mobile", usersearch.Mobile);
                cmd.Parameters.AddWithValue("@Gender", usersearch.Gender);
                cmd.Parameters.AddWithValue("@MaritalStatus", usersearch.MaritalStatus);
                cmd.Parameters.AddWithValue("@PageNumber", usersearch.PageNumber);
                cmd.Parameters.AddWithValue("@PageSize", usersearch.PageSize);
                Con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);

                list = new List<UserTable3>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    UserTable3 userTable = new UserTable3();
                    userTable.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                    userTable.Name = ds.Tables[0].Rows[i]["Name"].ToString();
                    userTable.Email = ds.Tables[0].Rows[i]["Email"].ToString();
                    userTable.Mobile = ds.Tables[0].Rows[i]["Mobile"].ToString();
                    userTable.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["CreatedDate"].ToString());
                    userTable.Gender = ds.Tables[0].Rows[i]["Gender"].ToString();
                    userTable.MaritalStatus = ds.Tables[0].Rows[i]["MaritalStatus"].ToString();
                    userTable.ProfileImage = ds.Tables[0].Rows[i]["ProfileImage"].ToString();
                    int PageSize =  Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    int TotalPages = Convert.ToInt32(ds.Tables[0].Rows[i]["TotalPages"].ToString());
                    userTable.TotalPages = Convert.ToInt32(Math.Ceiling((double) TotalPages / PageSize));
                    userTable.PageSize = Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    userTable.PageNumber = Convert.ToInt32(ds.Tables[0].Rows[i]["PageNumber"].ToString());
                    userTable.CurrentPage = Convert.ToInt32(ds.Tables[0].Rows[i]["CurrentPage"].ToString());
                    list.Add(userTable);
                }
                return list;


            }

            catch
            {
                return list;
            }
            finally
            {
                Con.Close();
            }
        }
    }
}
