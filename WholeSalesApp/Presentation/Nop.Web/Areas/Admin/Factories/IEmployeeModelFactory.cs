﻿
using Nop.Core.Domain.Catalog;
using System.Collections.Generic;

namespace Nop.Services.EmployeeModelFactory
{
    public partial interface IEmployeeModelFactory
    {

        List<Employee_New> GetEmployee();

        Employee_New GetEmployeeSelect(Employee_New emp);

        void InsertEmpRecord(Employee_New emp);
        Employee_New UpdateEmpRecord(Employee_New emp);
        void DeleteEmpRecord(Employee_New empid);

    }
}

