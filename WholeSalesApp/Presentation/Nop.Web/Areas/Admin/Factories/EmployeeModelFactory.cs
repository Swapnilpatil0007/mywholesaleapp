﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.EmployeeModelFactory
{
    public partial class EmployeeModelFactory : IEmployeeModelFactory
    {
        private readonly IRepository<Employee_New> _ListEmployeemodelRepository;
        public EmployeeModelFactory(IRepository<Employee_New> ListEmployeemodelRepository)
        {
            _ListEmployeemodelRepository = ListEmployeemodelRepository;
        }

        List<Employee_New> IEmployeeModelFactory.GetEmployee()
        {
            var query = from p in _ListEmployeemodelRepository.Table select p;
            var detail = query.ToList();

            return detail;
        }

        public void InsertEmpRecord(Employee_New emp)
        {
            emp.Id = 0;
            _ListEmployeemodelRepository.Insert(emp);
            var query = from p in _ListEmployeemodelRepository.Table select p;
        }

        public Employee_New GetEmployeeSelect(Employee_New emp)
        {
            var query = from p in _ListEmployeemodelRepository.Table where p.Id == emp.Id select p;
            var detail = query.ToList().FirstOrDefault();
            return detail;
        }

        public Employee_New UpdateEmpRecord(Employee_New emp)
        {
           _ListEmployeemodelRepository.Update(emp);
            var query = from p in _ListEmployeemodelRepository.Table select p;
            var detail = query.ToList().FirstOrDefault();
            return detail;
        }

        public void DeleteEmpRecord(Employee_New empid)
        {
            _ListEmployeemodelRepository.Delete(empid);
            var query = from p in _ListEmployeemodelRepository.Table where p.Id == empid.Id select p;
            //var detail = query.ToList();
            //return detail;
        }

        //public UpdateEmployee UpdateEmpRecord(UpdateEmployee emp)
        //{
        //    _ListEmployeemodelRepository.Update(emp);
        //    var query = from p in _ListEmployeemodelRepository.Table select p;
        //    var detail = query.ToList().FirstOrDefault();
        //    return detail;
        //}

        //public UpdateEmployee UpdateGetEmployeeSelect(UpdateEmployee emp)
        //{
        //    var query = from p in _ListEmployeemodelRepository.Table where p.Id == emp.Id select p;
        //    var detail = query.ToList().FirstOrDefault();
        //    return detail;
        //}
    }
}
