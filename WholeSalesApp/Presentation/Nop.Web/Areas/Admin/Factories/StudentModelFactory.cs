﻿using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace Nop.Web.Areas.Admin.Factories
{
    public  class StudentModelFactory :IStudentModelFactory
    {
        private readonly IRepository<Student> _StudentmodelRepository;
        public StudentModelFactory(IRepository<Student> StudentmodelRepository)
        {
            _StudentmodelRepository = StudentmodelRepository;
        }

        public void InsertStudRecord(Student stud)
        {
            _StudentmodelRepository.Insert(stud);
            var query = from p in _StudentmodelRepository.Table select p;
        }

        List<Student> IStudentModelFactory.GetStudents()
        {
            var query = from p in _StudentmodelRepository.Table select p;
            var detail = query.ToList();
            return detail;
        }

        public Student UpdateStudRecord(Student stud)
        {
            _StudentmodelRepository.Update(stud);
            var query = from p in _StudentmodelRepository.Table select p;
            var detail = query.ToList().FirstOrDefault();
            return detail;
        }

        public Student GetStudentSelect(Student stud)
        {
            var query = from p in _StudentmodelRepository.Table where p.Id == stud.Id select p;
            var detail = query.ToList().FirstOrDefault();
            return detail;
        }
        public List<Student3> StudentSearchModel(Student3 studentsearch)
        {
            SqlConnection Con = new SqlConnection();
            DataSet ds = null;
            List<Student3> list = null;

            try
            {


                Con = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                SqlCommand cmd = new SqlCommand("Sp_StudentSearch", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", studentsearch.Name);
                cmd.Parameters.AddWithValue("@Email", studentsearch.Email);
                cmd.Parameters.AddWithValue("@Salary", studentsearch.Salary);
                cmd.Parameters.AddWithValue("@PageNumber", studentsearch.PageNumber);
                cmd.Parameters.AddWithValue("@PageSize", studentsearch.PageSize);
                Con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);

                list = new List<Student3>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Student3 studdata = new Student3();
                    studdata.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                    studdata.Name = ds.Tables[0].Rows[i]["Name"].ToString();
                    studdata.Email = ds.Tables[0].Rows[i]["Email"].ToString();
                    studdata.Salary = Convert.ToDecimal(ds.Tables[0].Rows[i]["Salary"].ToString());
                    studdata.ProfileImage = ds.Tables[0].Rows[i]["ProfileImage"].ToString();
                    int PageSize = Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    int TotalPages = Convert.ToInt32(ds.Tables[0].Rows[i]["TotalPages"].ToString());
                    studdata.TotalPages = Convert.ToInt32(Math.Ceiling((double)TotalPages / PageSize));
                    studdata.PageSize = Convert.ToInt32(ds.Tables[0].Rows[i]["PageSize"].ToString());
                    studdata.PageNumber = Convert.ToInt32(ds.Tables[0].Rows[i]["PageNumber"].ToString());
                    studdata.CurrentPage = Convert.ToInt32(ds.Tables[0].Rows[i]["CurrentPage"].ToString());
                    list.Add(studdata);
                }
                return list;


            }

            catch
            {
                return list;
            }
            finally
            {
                Con.Close();
            }
        }
    }
}
