﻿using Nop.Core.Domain.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    public interface IStudentModelFactory
    {
        List<Student> GetStudents();

        void InsertStudRecord(Student stud);
        Student UpdateStudRecord(Student stud);

        Student GetStudentSelect(Student stud);

        List<Student3> StudentSearchModel(Student3 studentsearch);
    }
}
