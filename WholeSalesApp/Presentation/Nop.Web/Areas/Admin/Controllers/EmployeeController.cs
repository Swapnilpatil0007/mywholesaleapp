﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.EmployeeModelFactory;
using Nop.Services.Media;
using System;
using System.IO;

namespace Nop.Web.Areas.Admin.Controllers
{

    public partial class EmployeeController : BaseAdminController
    {
        #region Fields

        private readonly IEmployeeModelFactory _employee;
        private readonly INopFileProvider _fileProvider;

        #endregion
        #region Ctor

        public EmployeeController(
            IEmployeeModelFactory employee,
            INopFileProvider fileProvider)
        {
            this._employee = employee;
            this._fileProvider = fileProvider;
        }

        #endregion

        public virtual IActionResult List()
        {
            var employee = _employee.GetEmployee();

            return View(employee);
        }
        [HttpGet]
        public virtual IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public virtual IActionResult Create(Employee_New1 emp)
        {
            if (ModelState.IsValid)
            {
                var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

                string filePath = "";
                var uniqueFileName = "";
                if (emp.ProfileImage != null)
                {
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + emp.ProfileImage.FileName;
                    filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        emp.ProfileImage.CopyTo(fileStream);
                    }
                }

                Employee_New employee_New = new Employee_New();
                {
                    employee_New.Name = emp.Name;
                    employee_New.Email = emp.Email;
                    employee_New.Salary = emp.Salary;
                    employee_New.ProfileImage = uniqueFileName;
                };
                _employee.InsertEmpRecord(employee_New);
            }
            //return RedirectToAction("List");
            return View();
        }
        [HttpGet]
        public virtual IActionResult Edit(Employee_New1 emp)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniqueFileName = "";
            if (emp.ProfileImage != null)
            {
                uniqueFileName = Guid.NewGuid().ToString() + "_" + emp.ProfileImage.FileName;
                filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    emp.ProfileImage.CopyTo(fileStream);
                }
            }

            Employee_New employee_New = new Employee_New();
            employee_New.Id = emp.Id;

            var data = _employee.GetEmployeeSelect(employee_New);
            emp.Id = data.Id;
            emp.Name = data.Name;
            emp.Email = data.Email;
            emp.Salary = data.Salary;
            emp.ProfileImage1 = data.ProfileImage;
            return View(emp);
        }
        [HttpPost]
        public virtual IActionResult Update(Employee_New1 emp)
        {

            Employee_New employee_New = new Employee_New();
            employee_New.Id = emp.Id;
            employee_New.Name = emp.Name;
            employee_New.Email = emp.Email;
            employee_New.Salary = emp.Salary;
            employee_New.ProfileImage = emp.ProfileImage1;
            try
            {

                if (emp.ProfileImage != null)
                {
                    if (emp.ProfileImage.FileName != null)
                    {
                        var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

                        string filePath = "";
                        var uniqueFileName = "";
                        if (emp.ProfileImage != null)
                        {
                            uniqueFileName = Guid.NewGuid().ToString() + "_" + emp.ProfileImage.FileName;
                            filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                emp.ProfileImage.CopyTo(fileStream);
                            }
                        }
                        employee_New.ProfileImage = uniqueFileName;
                    }
                }
                else
                {
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                _employee.UpdateEmpRecord(employee_New);
            }
            return RedirectToAction("List");
        }

        public virtual IActionResult Delete(Employee_New empid)
        {
            _employee.DeleteEmpRecord(empid);
            return RedirectToAction("List");
        }
    }

}
