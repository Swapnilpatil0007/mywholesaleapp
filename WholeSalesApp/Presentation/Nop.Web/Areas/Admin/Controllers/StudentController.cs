﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using Nop.Web.Areas.Admin.Factories;
using System;
using System.IO;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class StudentController : BaseAdminController
    {
        private readonly IStudentModelFactory _student;
        private readonly IPictureService _pictureService;
        private readonly INopFileProvider _fileProvider;
        public StudentController(IStudentModelFactory student,
            IPictureService pictureService, INopFileProvider fileProvider)
        {
            this._student = student;
            this._pictureService = pictureService;
            this._fileProvider = fileProvider;
        }

        public virtual IActionResult List()
        {
            var students = _student.GetStudents();
            return View(students);
        }
        public IActionResult ListData(Studentpass studlist)
        {

            Student3 searchstud = new Student3();
            searchstud.PageNumber = studlist.PageNumber;
            searchstud.PageSize = studlist.PageSize;
            searchstud.CurrentPage = studlist.PageNumber;

            if (studlist.Name != null && studlist.Name != "")
            {
                searchstud.Name = studlist.Name;
            }
            if (studlist.Email != null && studlist.Email != "")
            {
                searchstud.Email = studlist.Email;
            }
            //if (studlist.Salary != null && studlist.Salary != "")
            //{
            //    searchstud.Salary = studlist.Salary;
            //}

            var details = _student.StudentSearchModel(searchstud);
            return Json(details);
        }
        [HttpGet]
        public virtual IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public virtual IActionResult Create(Student_New1 stud)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniquefilename = "";
            if (stud.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + stud.ProfileImage.FileName;
                filePath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filePath, FileMode.Create))
                {
                    stud.ProfileImage.CopyTo(filestream);
                }
            }
            Student student = new Student();
            student.Name = stud.Name;
            student.Email = stud.Email;
            student.Salary = stud.Salary;
            student.ProfileImage = uniquefilename;

            _student.InsertStudRecord(student);
            return RedirectToAction("List");
        }

        public virtual IActionResult Edit(Student_New1 stud)
        {
            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniquefilename = "";
            if (stud.ProfileImage != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "_" + stud.ProfileImage.FileName;
                filePath = Path.Combine(thumbsDirectoryPath, uniquefilename);
                using (var filestream = new FileStream(filePath, FileMode.Create))
                {
                    stud.ProfileImage.CopyTo(filestream);
                }
            }
            Student student = new Student();
            student.Id = stud.Id;

            var data = _student.GetStudentSelect(student);

            stud.Id = data.Id;
            stud.Name = data.Name;
            stud.Email = data.Email;
            stud.Salary = data.Salary;
            stud.ProfileImage1 = data.ProfileImage;
            return View(stud);
        }
        public virtual IActionResult Update(Student_New1 stud)
        {
            Student student = new Student();
            student.Id = stud.Id;
            student.Name = stud.Name;
            student.Email = stud.Email;
            student.Salary = stud.Salary;
            student.ProfileImage = stud.ProfileImage1;

            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

            string filePath = "";
            var uniqueFileName = "";

            if (stud.ProfileImage != null)
            {
                if (stud.ProfileImage.FileName != null)
                {
                    if (stud.ProfileImage != null)
                    {
                        uniqueFileName = Guid.NewGuid().ToString() + "_" + stud.ProfileImage.FileName;
                        filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            stud.ProfileImage.CopyTo(fileStream);
                        }
                        student.ProfileImage = uniqueFileName;
                    }
                }

            }

            _student.UpdateStudRecord(student);
            return RedirectToAction("List");
        }
        //public virtual IActionResult Delete(Student studid)
        //{
        //    _student.DeleteEmpRecord(studid);
        //    return RedirectToAction("List");
        //}
    }
}
