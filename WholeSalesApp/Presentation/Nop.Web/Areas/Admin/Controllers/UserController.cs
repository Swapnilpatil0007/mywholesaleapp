﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Customers;
using System;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class UserController : BaseAdminController
    {
        #region Fields

        private readonly IUserModelFactory _users;
        private readonly INopFileProvider _fileProvider;

        #endregion
        #region Ctor

        public UserController(
            IUserModelFactory users,
            INopFileProvider fileProvider)
        {
            this._users = users;
            this._fileProvider = fileProvider;
        }

        #endregion
        //public virtual ActionResult AddEmployee()
        //{

        //    return View();
        //}
        ////Post method to add details    
        //[HttpPost]
        //public virtual ActionResult AddEmployee(TestTable obj)
        //{
        //    _users.AddDetails(obj);

        //    return View();
        //}
        

        [HttpGet]
        public virtual IActionResult Test()
        {
            return View();
        }
        [HttpPost]
        public virtual IActionResult Test(TestTable obj)
        {
            if (!string.IsNullOrWhiteSpace(obj.Mobile) && _users.GetUserMobile(obj.Mobile) != null)
            {
                ModelState.AddModelError(string.Empty, "Mobile is already registered");
            }
            if ((string.IsNullOrWhiteSpace(obj.Mobile) && _users.GetUserMobile(obj.Mobile) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Enter Mobile Number..!");
            }
            if (ModelState.IsValid)
            {
                _users.AddDetails(obj);
                //TempData["Msg"] = "User Added Successfully.";
                return View();
            }
            return View();
        }
       
        public virtual IActionResult List()
        {
            return View();
        }
        public IActionResult ListData(UserTablepass user)
        {
         
            UserTable3 searchuser1 = new UserTable3();
            searchuser1.PageNumber = user.PageNumber;
            searchuser1.PageSize = user.PageSize;
            searchuser1.CurrentPage = user.PageNumber;

            if (user.Name != null && user.Name != "")
            {
                searchuser1.Name = user.Name;
            }
            if (user.Email != null && user.Email != "")
            {
                searchuser1.Email = user.Email;
            }
            if (user.Mobile != null && user.Mobile != "")
            {
                searchuser1.Mobile = user.Mobile;
            }
            if (user.MaritalStatus != null && user.MaritalStatus != "")
            {
                searchuser1.MaritalStatus = user.MaritalStatus;
            }
            if (user.Gender != null && user.Gender != "")
            {
                searchuser1.Gender = user.Gender;
            }

            var details = _users.UserSearchModel(searchuser1);
            return Json(details);
        }
        [HttpGet]
        public virtual IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public virtual IActionResult Create(UserTable1 user)
        {
            if (!string.IsNullOrWhiteSpace(user.Email) && _users.GetUserByEmail(user.Email) != null)
            {
                ModelState.AddModelError(string.Empty, "Email is already registered");
            }
            else
            if ((string.IsNullOrWhiteSpace(user.Email) && _users.GetUserByEmail(user.Email) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Enter Email_Id..!");
            }
            if (!string.IsNullOrWhiteSpace(user.Mobile) && _users.GetUserByMobile(user.Mobile) != null)
            {
                ModelState.AddModelError(string.Empty, "Mobile is already registered");
            }
            if ((string.IsNullOrWhiteSpace(user.Mobile) && _users.GetUserByEmail(user.Mobile) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Enter Mobile Number..!");
            }
            if ((string.IsNullOrWhiteSpace(user.MaritalStatus) && _users.GetUserByEmail(user.MaritalStatus) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Select Marital Status..!");
            }
            if (user.ProfileImage == null)
            {
                ModelState.AddModelError(string.Empty, "Please Select ProfileImage..!");
            }
            if (ModelState.IsValid)
            {
                var DirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);
                var uniquefilename = "";
                if (user.ProfileImage != null)
                {
                    uniquefilename = Guid.NewGuid().ToString() + "_" + user.ProfileImage.FileName;
                    var filePath = Path.Combine(DirectoryPath, uniquefilename);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        user.ProfileImage.CopyTo(fileStream);
                    }
                }
                UserTable userdata = new UserTable();
                {
                    userdata.Name = user.Name;
                    userdata.Email = user.Email;
                    userdata.Mobile = user.Mobile;
                    userdata.CreatedDate = DateTime.UtcNow;
                    userdata.Gender = user.Gender;
                    userdata.MaritalStatus = user.MaritalStatus;
                    userdata.ProfileImage = uniquefilename;
                }
                _users.InsertUserRecord(userdata);
                return RedirectToAction("List");
            }
            return View();
        }
        [HttpGet]
        public virtual IActionResult Edit(UserTable1 user)
        {
            UserTable user1 = new UserTable();
            user1.Id = user.Id;
            
            if (user.Id > 0)
            {
                var data = _users.GetUserSelect(user1);
                user.Name = data.Name;
                user.Email = data.Email;
                user.Mobile = data.Mobile;
                user.Gender = data.Gender;
                user.MaritalStatus = data.MaritalStatus;
                user.ProfileImage1 = data.ProfileImage;

                return View(user);
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public virtual IActionResult Edit(UserTable1 user,int Id = 0)
        {

            //if(user.Name == null || user.Name == "")
            //{
            //    return RedirectToAction("List");
            //}
            if ((string.IsNullOrWhiteSpace(user.Email) && _users.GetUserByEmail(user.Email) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Enter Email_Id..!");
            }
            if ((string.IsNullOrWhiteSpace(user.Mobile) && _users.GetUserByEmail(user.Mobile) == null))
            {
                ModelState.AddModelError(string.Empty, "Please Enter Mobile Number..!");
            }
            if (ModelState.IsValid)
            {
                UserTable userdata = new UserTable();
                {
                    userdata.Id = user.Id;
                    userdata.Name = user.Name;
                    userdata.Email = user.Email;
                    userdata.Mobile = user.Mobile;
                    userdata.CreatedDate = DateTime.UtcNow;
                    userdata.Gender = user.Gender;
                    userdata.MaritalStatus = user.MaritalStatus;
                    userdata.ProfileImage = user.ProfileImage1;
                }

                try
                {

                    if (user.ProfileImage != null)
                    {
                        if (user.ProfileImage.FileName != null)
                        {
                            var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(NopMediaDefaults.ImageThumbsPath);

                            string filePath = "";
                            var uniqueFileName = "";
                            if (user.ProfileImage != null)
                            {
                                uniqueFileName = Guid.NewGuid().ToString() + "_" + user.ProfileImage.FileName;
                                filePath = Path.Combine(thumbsDirectoryPath, uniqueFileName);
                                using (var fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    user.ProfileImage.CopyTo(fileStream);
                                }
                            }
                            userdata.ProfileImage = uniqueFileName;
                        }
                    }
                    else
                    {
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    _users.UpdateUserRecord(userdata);
                }
                return RedirectToAction("List");
            }
            //else if (user.ProfileImage1 == null)
            //{
            //    UserTable user1 = new UserTable();
            //    user1.Id = user.Id;
            //    var data = _users.GetUserSelect(user1);

            //    data.Id = user.Id;
            //    data.Name = user.Name;
            //    data.Email = user.Email;
            //    data.Mobile = user.Mobile;
            //    data.CreatedDate = DateTime.UtcNow;
            //    data.Gender = user.Gender;
            //    data.MaritalStatus = user.MaritalStatus;
            //    data.ProfileImage = user.ProfileImage1;

            //    _users.UpdateUserRecord(data);
            //    return RedirectToAction("List");
            //}

            if (user.Id > 0)
            {
                UserTable user1 = new UserTable();
                user1.Id = user.Id;

                var data = _users.GetUserSelect(user1);


                user.Name = data.Name;
                user.Email = data.Email;
                user.Mobile = data.Mobile;
                user.Gender = data.Gender;
                user.MaritalStatus = data.MaritalStatus;
                user.ProfileImage1 = data.ProfileImage;

                return View(user);
            }
           

            return View();
        }
        public virtual IActionResult Delete(UserTable userid)
        {
            try
            {
                var data = _users.DeleteUserRecord(userid);
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
