﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Nop.Core.Data;

namespace Nop.Web.Areas.Admin.Helpers
{
    public class DirectSQLCommand
    {


        public static DataTable ExecuteProcedureDataTable(string ProcedureName, SqlParameter[] param)
        {

            
            SqlConnection connection = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dadpt = new SqlDataAdapter();
            DataTable dt = new DataTable();

            try
            {
                connection = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                cmd = new SqlCommand(ProcedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 300;
                AttachParameters(cmd, param);
                dadpt = new SqlDataAdapter();
                dadpt.SelectCommand = cmd;
                dt = new DataTable();
                dadpt.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                dadpt.Dispose();
                cmd.Dispose();
                dt.Dispose();
            }

        }

        public static DataSet ExecuteProcedureDataSet(string ProcedureName, SqlParameter[] param)
        {

            SqlConnection connection = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dadpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {


                connection = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                cmd = new SqlCommand(ProcedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 300;
                AttachParameters(cmd, param);
                dadpt.SelectCommand = cmd;
                dadpt.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
                dadpt.Dispose();
                ds.Dispose();
            }

        }

        public static DataTable ExecuteSelectQueryDataTable(string query)
        {
            DataSettings dataSettings = new DataSettings();
            SqlConnection connection = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dadpt = new SqlDataAdapter();
            DataTable dt = new DataTable();

            try
            {
                connection = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
                cmd = new SqlCommand(query, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 300;
                dadpt = new SqlDataAdapter();
                dadpt.SelectCommand = cmd;
                dt = new DataTable();
                dadpt.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                dadpt.Dispose();
                cmd.Dispose();
                dt.Dispose();
            }

        }

        static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (commandParameters == null)
            {
                return;
            }
            foreach (SqlParameter p in commandParameters)
            {
                if (p == null)
                    continue;
                if ((p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Input) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }
                command.Parameters.Add(p);
            }
        }
        public static List<T> ConvertToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row => {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name.ToLower()))
                    {
                        try
                        {
                            pro.SetValue(objT, row[pro.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }
    }
}
