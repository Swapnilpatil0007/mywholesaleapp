﻿using Newtonsoft.Json;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Nop.Web.Areas.Admin.Helpers
{
    public static class BankHelper 
    {

        public class SelectListItem
        {
            public bool Disabled  { get; set; }
            public string Group  { get; set; }
            public bool Selected  { get; set; }
            public string Text  { get; set; }
            public string Value { get; set; }
        }
        public static IList<SelectListItem> BankList(IList<SelectListItem> selectListItems)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in selectListItems)
            {
                list.Add(new SelectListItem() { Disabled = item.Disabled, Group = item.Group, Selected = item.Selected, Text = item.Text, Value = item.Value });
            }
            list.Add(new SelectListItem() { Disabled = false, Group = null, Selected =false, Text = "CB BANK", Value = "BANK" });
            list.Add(new SelectListItem() { Disabled = false, Group = null, Selected =false, Text = "AYA BANK", Value = "BANK" });
            list.Add(new SelectListItem() { Disabled = false, Group = null, Selected =false, Text = "KBZ BANK", Value = "BANK" });
            list.Add(new SelectListItem() { Disabled = false, Group = null, Selected =false, Text = "MAB BANK", Value = "BANK" });
            list.Add(new SelectListItem() { Disabled = false, Group = null, Selected =false, Text = "UAB BANK", Value = "BANK" });
            return list;
        }

    }
}
