﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Nop.Web.Areas.Admin.Helpers
{
    /// <summary>
    /// Select list helper  
    /// </summary>
    public class Productqtycheck
    {   
        private readonly IConfiguration _configuration;
        public Productqtycheck(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public bool checkqtyProduct(int productid)            
        {
            bool result = new bool();
            string joined = string.Join(",", productid);
            SqlParameter[] aSqlParameter = new SqlParameter[1];
            aSqlParameter[0] = new SqlParameter("@productid", joined);
            DataTable dt = DirectSQLCommand.ExecuteProcedureDataTable("Product_List", aSqlParameter);
            var emp = (from DataRow row in dt.Rows
                       select new
                       {
                           erpProductId = row["CGMItemID"].ToString(),
                       }).ToList();

            string json = JsonConvert.SerializeObject(emp, Formatting.Indented);
            string GETERPApi = _configuration.GetValue<string>("ERPApi1:Api1");
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GETERPApi);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            System.Net.ServicePointManager.Expect100Continue = false;
            try
            {
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var responseString = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                var myDeserializedClass = JsonConvert.DeserializeObject<List<RootErpQTY>>(responseString);
                if (myDeserializedClass != null && myDeserializedClass.Count > 0)
                {
                    var checklessqty = myDeserializedClass.Where(b => b.IsAvailabe == false);
                    if (checklessqty != null && checklessqty.Count() > 0)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public class RootErpQTY
        {
            public string ProductBarcode { get; set; }
            public string ERPProductId { get; set; }
            public string ProductName { get; set; }
            public int Qty { get; set; }
            public int AvailableQty { get; set; }
            public bool IsAvailabe { get; set; }
            public double Rate { get; set; }
        }
    }
}