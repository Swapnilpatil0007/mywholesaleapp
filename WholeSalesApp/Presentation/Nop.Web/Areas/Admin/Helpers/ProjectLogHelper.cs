﻿using Nop.Core.Data;
using System.Data;
using System.Data.SqlClient;

namespace Nop.Web.Areas.Admin.Helpers
{
    public static class ProjectLogHelper
    {
        /// <summary>
        /// LogHelper
        /// </summary>
        /// <param name="GETERPApi">url</param>
        /// <param name="json">json</param>
        /// <param name="responseString">responseString</param>
        /// <param name="CreateItemId">responseString</param>
        public static int LogHelper(string GETERPApi, string json, string responseString, string CreateItemId)
        {
            SqlConnection Con = new SqlConnection();
            Con = new SqlConnection(DataSettingsManager.LoadSettings()?.DataConnectionString);
            SqlCommand com = new SqlCommand("Sp_logAdd", Con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@SURL", GETERPApi);
            com.Parameters.AddWithValue("@Postdata", json);
            com.Parameters.AddWithValue("@Response", responseString);
            com.Parameters.AddWithValue("@CreateItemId", CreateItemId);
            Con.Open();
            int i = com.ExecuteNonQuery();
            Con.Close();
            return i;
        }
    }
}
