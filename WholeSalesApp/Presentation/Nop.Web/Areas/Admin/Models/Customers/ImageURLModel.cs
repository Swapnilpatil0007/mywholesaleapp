﻿using Nop.Web.Framework.Models;


namespace Nop.Web.Areas.Admin.Models.Customers
{
    public partial class ImageURLModel 
    {
        public string label { get; set; }
        public string ImageURL  { get; set; }
    }
}
