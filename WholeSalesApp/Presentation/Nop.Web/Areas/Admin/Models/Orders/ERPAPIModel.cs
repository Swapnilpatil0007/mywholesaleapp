﻿using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Orders
{

    public partial class RootObjectModel
    {
        public List<POSSalesInvoiceDetailModel> POSSalesInvoiceDetail { get; set; }
        public POSSalesInvoiceHeaderModel POSSalesInvoiceHeaderModel { get; set; }
        public List<SalesInvoicePayment> SalesInvoicePayment { get; set; }
    }
    public partial class SalesInvoicePayment
    {
        public string CompanyId { get; set; }
        public string BusinessUnitId { get; set; }
        public string PaymentTypeId { get; set; }
        public string CardType { get; set; }
        public string BankId { get; set; }
        public string CardNo { get; set; }
        public string TransactionNo { get; set; }
        public decimal Amount { get; set; }


    }

    public partial class POSSalesInvoiceHeaderModel
    {
        //public List<POSSalesInvoiceDetailModel> POSSalesInvoiceDetail { get; set; }

        //public int Id { get; set; }
        public string CompanyId { get; set; }
        public string BusinessUnitId { get; set; }
        public string InvoiceDate { get; set; }
        public decimal TotalAmount { get; set; }
        public string SalesInvoiceCode { get; set; }

        public bool IsWholeSale { get; set; }
        public string Remarks { get; set; }

    }


    public partial class POSSalesInvoiceDetailModel
    {
        // public string Id { get; set; }

        public string ProductId { get; set; }
        //public string PickUpAddress { get; set; }
        public int Quantity { get; set; }

        public decimal SellingRate { get; set; }
        public int ecommid { get; set; }


    }
}
