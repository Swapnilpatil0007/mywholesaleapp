﻿using System.Collections.Generic;


namespace Nop.Web.Areas.Admin.Models.Employee 
{
    public partial class EmployeeListmodel 
    {
        
        public List<ListEmployeemodel> ListEmployeemodeldata { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
    }

    public partial class ListEmployeemodel 
    {
      
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
    }
}
