﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Common
{
    public class BankListModel 
    {
        public string id  { get; set; }
        public string bankname  { get; set; }
        public string branchname  { get; set; }
        public string accno_normal  { get; set; }
        public string accno_special  { get; set; }
        public string remark  { get; set; }
    }
}
