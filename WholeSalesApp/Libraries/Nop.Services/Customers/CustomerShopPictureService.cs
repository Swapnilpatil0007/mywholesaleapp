using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Xml;
using Microsoft.EntityFrameworkCore;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Data.Extensions;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.CustomerShopPicture;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Customer service
    /// </summary>
    public partial class CustomerShopPictureService : ICustomerShopPictureService
    {
        #region Fields
        private readonly IRepository<CustomerShopPicture> _CustomerShopPictureRepository;
        #endregion

        public CustomerShopPictureService(IRepository<CustomerShopPicture> CustomerShopPictureRepository)
        {
            this._CustomerShopPictureRepository = CustomerShopPictureRepository;
            
        }


        #region Customers

        public virtual void InsertCustomerShopPicture(CustomerShopPicture model)
        {
               if (model == null)
                    throw new ArgumentNullException(nameof(model));

                _CustomerShopPictureRepository.Insert(model);
           
        }

        #endregion
        
        
    }
}