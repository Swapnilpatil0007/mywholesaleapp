using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.CustomerShopPicture;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Customer service interface
    /// </summary>
    public partial interface ICustomerShopPictureService
    {
        #region Customers

        void InsertCustomerShopPicture(CustomerShopPicture customer);

        #endregion


    }
}