using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Services.Discounts;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Parameters for the updating order totals
    /// </summary>
    public class UpdateOrderParameters
    {
        public UpdateOrderParameters()
        {
            Warnings = new List<string>();
            AppliedDiscounts = new List<DiscountForCaching>();
        }

        /// <summary>
        /// The updated order
        /// </summary>
        public Order UpdatedOrder { get; set; }

        /// <summary>
        /// The updated order item
        /// </summary>
        public OrderItem UpdatedOrderItem { get; set; }

        /// <summary>
        /// The price of item with tax
        /// </summary>
        public decimal PriceInclTax { get; set; }

        /// <summary>
        /// The price of item without tax
        /// </summary>
        public decimal PriceExclTax { get; set; }

        /// <summary>
        /// The quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The amount of discount with tax
        /// </summary>
        public decimal DiscountAmountInclTax { get; set; }

        /// <summary>
        /// The amount of discount without tax
        /// </summary>
        public decimal DiscountAmountExclTax { get; set; }

        /// <summary>
        /// Subtotal of item with tax
        /// </summary>
        public decimal SubTotalInclTax { get; set; }

        /// <summary>
        /// Subtotal of item without tax
        /// </summary>
        public decimal SubTotalExclTax { get; set; }

        /// <summary>
        /// Warnings
        /// </summary>
        public List<string> Warnings { get; set; }

        /// <summary>
        /// Applied discounts
        /// </summary>
        public List<DiscountForCaching> AppliedDiscounts { get; set; }

        /// <summary>
        /// Pickup point
        /// </summary>
        public PickupPoint PickupPoint { get; set; }
    }


    public class DeleteERPIinvoice
    {
        public string POSSalesInvoiceCode { get; set; }
        
    }



    public partial class ERPSalesInvoiceDetailModel
    {
        // public string Id { get; set; }

        public string ProductId { get; set; }
        //public string PickUpAddress { get; set; }
        public int Quantity { get; set; }

        public decimal SellingRate { get; set; }
        public int ecommid { get; set; }


    }


    public partial class ERPSalesInvoiceHeaderModel
    {
        //public List<POSSalesInvoiceDetailModel> POSSalesInvoiceDetail { get; set; }

        //public int Id { get; set; }
        public string CompanyId { get; set; }
        public string BusinessUnitId { get; set; }
        public string InvoiceDate { get; set; }
        public decimal TotalAmount { get; set; }
        public string SalesInvoiceCode { get; set; }

        public bool IsWholeSale { get; set; }
        public string Remarks { get; set; }

    }



    public partial class ErpSalesInvoicePayment
    {
        public string CompanyId { get; set; }
        public string BusinessUnitId { get; set; }
        public string PaymentTypeId { get; set; }
        public string CardType { get; set; }
        public string BankId { get; set; }
        public string CardNo { get; set; }
        public string TransactionNo { get; set; }
        public decimal Amount { get; set; }


    }


    public partial class ErpRootObjectModel
    {
        public List<ERPSalesInvoiceDetailModel> POSSalesInvoiceDetail { get; set; }
        public ERPSalesInvoiceHeaderModel POSSalesInvoiceHeaderModel { get; set; }

        public List<ErpSalesInvoicePayment> SalesInvoicePayment { get; set; }
    }
}