﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.CustomerShopPicture
{

    public partial class CustomerShopPicture : BaseEntity
    {


        public CustomerShopPicture()
        {

        }
        /// <summary>
        /// Gets or sets the username
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets a value for IsDisplayEmail
        /// </summary>
        public string ImageURL { get; set; }

    }
}
