﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Directory
{
    public partial class IBankSelectListItem : BaseEntity, ILocalizedEntity
    {
        public bool Disabled { get; set; }
        public string Group { get; set; }
        public bool Selected { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
