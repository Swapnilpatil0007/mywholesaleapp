﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Nop.Core.Domain.Catalog
{
    public partial class EmployeeListmodel
    {

        public List<Employee_New> ListEmployeemodeldata { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
        public string ProfileImage { get; set; }
    }
    public partial class Employee_New : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Salary { get; set; }
        public string ProfileImage { get; set; }

    }
    public partial class Employee_New1 
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Salary is required")]
        public decimal Salary { get; set; }
        [Required(ErrorMessage = "Image is required")]
        public IFormFile ProfileImage { get; set; }
        public string ProfileImage1 { get; set; }
    }
}
