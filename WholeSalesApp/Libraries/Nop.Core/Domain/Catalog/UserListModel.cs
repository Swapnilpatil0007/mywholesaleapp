﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Nop.Core.Domain.Catalog
{
    public partial class UserListModel
    {

        public List<TestTable> ListEmployeemodeldata { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }

        public string Mobile { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Gender { get; set; }

        public string MaritalStatus { get; set; }
        public string ProfileImage { get; set; }
    }
    public partial class UserTable : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }

        public string Mobile { get; set; }

        public DateTime? CreatedDate { get; set; }
        public string Gender { get; set; }

        public string MaritalStatus { get; set; }
        public string ProfileImage { get; set; }
    }
    public partial class TestTable : BaseEntity
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public string Mobile { get; set; }
    }
    public partial class UserTable1
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public IFormFile ProfileImage { get; set; }
        public string ProfileImage1 { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalPages { get; set; }
    }
    public partial class UserTable3
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string ProfileImage { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalPages { get; set; }

        public int CurrentPage { get; set; }
    }
    public partial class UserTablepass
    {

        public string Name { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }
        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalPages { get; set; }
    }
}
