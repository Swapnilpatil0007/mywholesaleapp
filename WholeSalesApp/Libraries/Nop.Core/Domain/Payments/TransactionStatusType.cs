﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Payments
{
    public enum TransactionStatusType
    {
        Success = 1,
        Failed = 2,
        Pending = 3
    }
}
