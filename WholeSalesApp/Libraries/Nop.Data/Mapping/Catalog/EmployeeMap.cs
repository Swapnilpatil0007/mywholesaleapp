﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a category mapping configuration
    /// </summary>
    public partial class EmployeeMap : NopEntityTypeConfiguration<Employee_New>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Employee_New> builder)
        {
            builder.ToTable(nameof(Employee_New));
            builder.HasKey(Employee_New => Employee_New.Id);
            builder.Property(Employee_New => Employee_New.Name).HasMaxLength(400).IsRequired();
            builder.Property(Employee_New => Employee_New.Email).HasMaxLength(400);
            builder.Property(Employee_New => Employee_New.Salary).HasMaxLength(400);
            builder.Property(Employee_New => Employee_New.ProfileImage).HasMaxLength(400);
            base.Configure(builder);
        }

        #endregion
    }
}