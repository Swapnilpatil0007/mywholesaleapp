﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class StudentMap : NopEntityTypeConfiguration<Student>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable(nameof(Student));
            builder.HasKey(Student => Student.Id);
            builder.Property(Student => Student.Name).HasMaxLength(400).IsRequired();
            builder.Property(Student => Student.Email).HasMaxLength(400);
            builder.Property(Student => Student.Salary).HasMaxLength(400);
            builder.Property(Student => Student.ProfileImage).HasMaxLength(400);
            base.Configure(builder);
        }

        #endregion
    }
}
