﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Catalog
{
    public partial class TestTableMap : NopEntityTypeConfiguration<TestTable>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<TestTable> builder)
        {
            builder.ToTable(nameof(TestTable));
            builder.HasKey(UserTable => UserTable.Id);
            builder.Property(UserTable => UserTable.Name).HasMaxLength(400).IsRequired();
            builder.Property(UserTable => UserTable.Mobile).HasMaxLength(400);
            builder.Property(UserTable => UserTable.Gender).HasMaxLength(400);
            base.Configure(builder);
        }

        #endregion
    }
}
