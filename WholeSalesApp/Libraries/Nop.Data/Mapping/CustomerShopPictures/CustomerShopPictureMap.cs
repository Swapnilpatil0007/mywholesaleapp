﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.CustomerShopPicture;

namespace Nop.Data.Mapping.CustomerShopPictures
{
    

    public partial class CustomerShopPictureMap : NopEntityTypeConfiguration<CustomerShopPicture>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<CustomerShopPicture> builder)
        {
            builder.ToTable(nameof(CustomerShopPicture));
            builder.HasKey(CustomerShopPicture => CustomerShopPicture.Id);
            builder.Property(CustomerShopPicture => CustomerShopPicture.CustomerId);
            builder.Property(CustomerShopPicture => CustomerShopPicture.ImageURL);
            base.Configure(builder);
        }

        #endregion
    }
}
